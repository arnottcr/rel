package diff

import (
	"go/doc"
)

func CompareFunc(exp, act *doc.Func) bool {
	// TODO(arnottcr): compare more than just the names
	return exp.Name == exp.Name
}

func CompareType(exp, act *doc.Type) bool {
	// TODO(arnottcr): compare more than just the names
	return exp.Name == act.Name
}

func CompareValues(exp, act []*doc.Value) ([]*doc.Value, []*doc.Value) {
	for i := len(act) - 1; i >= 0; i-- {
	outer:
		for j := len(act[i].Names) - 1; j >= 0; j-- {
			for k := len(exp) - 1; k >= 0; k-- {
				for l := len(exp[k].Names) - 1; l >= 0; l-- {
					// TODO(arnottcr): compare more than just the names
					if act[i].Names[j] == exp[k].Names[l] {
						act[i].Names = append(act[i].Names[:j], act[i].Names[j+1:]...)
						exp[k].Names = append(exp[k].Names[:l], exp[k].Names[l+1:]...)
						continue outer
					}
				}
				if len(exp[k].Names) == 0 {
					exp = append(exp[:k], exp[k+1:]...)
					continue
				}
			}
		}
		if len(act[i].Names) == 0 {
			act = append(act[:i], act[i+1:]...)
			continue
		}
	}
	return exp, act
}
