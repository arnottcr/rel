package diff

import (
	"errors"
	"fmt"
	"go/ast"
	"go/doc"
	"go/parser"
	"go/token"
	"os"
	"path"
	"path/filepath"
	"strings"

	git "gopkg.in/src-d/go-git.v4"
	"gopkg.in/src-d/go-git.v4/plumbing"
	"gopkg.in/src-d/go-git.v4/plumbing/object"
)

func TagPackages(repo string, tag *plumbing.Reference) (map[string]*doc.Package, error) {
	r, err := git.PlainOpen(repo)
	if err != nil {
		return nil, fmt.Errorf("opening repository: %v", err)
	}
	t, err := r.TagObject(tag.Hash())
	var c *object.Commit
	switch err {
	case nil:
		c, err = t.Commit()
	case plumbing.ErrObjectNotFound:
		c, err = r.CommitObject(tag.Hash())
	default:
		return nil, fmt.Errorf("resolving tag type: %v", err)
	}
	if err != nil {
		return nil, fmt.Errorf("extracting commit: %v", err)
	}
	t2, err := c.Tree()
	if err != nil {
		return nil, fmt.Errorf("fetching tree: %v", err)
	}
	var pkgs = map[string]*ast.Package{}
	var fset = token.FileSet{}
	if err = t2.Files().ForEach(func(f *object.File) error {
		if strings.HasSuffix(f.Name, ".go") {
			r, err := f.Reader()
			defer r.Close()
			if err != nil {
				return fmt.Errorf("acquiring %s reader: %v", f.Name, err)
			}
			src, err := parser.ParseFile(&fset, f.Name, r, 0)
			if err != nil {
				return fmt.Errorf("processing %s: %v", f.Name, err)
			}
			name := path.Dir(f.Name)
			pkg, found := pkgs[name]
			if !found {
				pkg = &ast.Package{
					Name:  name,
					Files: make(map[string]*ast.File),
				}
				pkgs[name] = pkg
			}
			pkg.Files[f.Name] = src
		}
		return nil
	}); err != nil {
		return nil, fmt.Errorf("walking tree: %v", err)
	}
	var docs = map[string]*doc.Package{}
	for k, v := range pkgs {
		docs[k] = doc.New(v, "", 0)
	}
	return docs, nil
}

func HeadPackages(repo string) (map[string]*doc.Package, error) {
	var docs = map[string]*doc.Package{}
	return docs, filepath.Walk(repo, func(p string, info os.FileInfo, err error) error {
		if !info.IsDir() {
			return nil
		}
		b := path.Base(p)
		if b == ".git" || strings.HasPrefix(b, ".") || strings.HasPrefix(b, "_") {
			return filepath.SkipDir
		}
		pkgs, err := parser.ParseDir(&token.FileSet{}, p, nil, 0)
		if err != nil {
			return fmt.Errorf("parsing pkg dir: %v", err)
		}
		switch len(pkgs) {
		case 1:
			for _, p1 := range pkgs {
				docs[path.Clean(strings.TrimPrefix(p+"/", repo+"/"))] = doc.New(p1, "", 0)
			}
			fallthrough
		case 0:
			return nil
		default:
			return errors.New("returned multiple pkgs")
		}
	})
}
